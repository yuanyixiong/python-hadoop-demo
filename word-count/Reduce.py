import sys

temp_word = None
temp_sum = 0
for line in sys.stdin:
    array = line.strip().split('\t')
    if len(array) != 2:
        continue
    word, sum = array

    if temp_word == None:
        temp_word = word

    if temp_word != word:
        print temp_word, temp_sum
        temp_word = word
        temp_sum = 0

    temp_sum += int(sum)

print temp_word, temp_sum
