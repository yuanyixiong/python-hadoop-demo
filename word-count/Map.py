import re
import sys

for line in sys.stdin:
    words = line.strip().split(' ')
    for word in words:
        word=re.compile(r"[a-zA-Z]*").findall(word.strip())[0];
        if len(word.strip()) <= 0:
            continue
        print '\t'.join([word.strip(), '1'])
