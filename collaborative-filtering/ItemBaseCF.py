#!/usr/bin/python
# -*- coding: UTF-8 -*-

## 计算物品之间的相似度
def item_similarity(train):
    '''1.计算用户之间共同的物品数量'''
    intersection = dict()  # 交集:物品与物品之间相同的用户数据量 intersection[itemId][itemId]=用户数据量
    commonMultiple = dict()  # 开平方根的分母(公倍数):每个物品拥有的用户数量 commonMultiple[itemId]=用户数据量

    for userId, items in train.items():
        for i in items:
            if intersection.get(i, -1) == -1:
                intersection[i] = dict()
            if commonMultiple.get(i, -1) == -1:
                commonMultiple[i] = 0
            commonMultiple[i] += 1
            for j in items:
                if i == j:
                    continue
                elif intersection[i].get(j, -1) == -1:
                    intersection[i][j] = 0
                intersection[i][j] += 1

    '''2.得到最终的相似度矩阵'''
    import math
    similarity = dict()
    for i, ru in intersection.items():
        if similarity.get(i, -1) == -1:
            similarity[i] = dict()
        for j, cuv in ru.items():
            if similarity[i].get(j, -1) == -1:
                similarity[i][j] = 0
            '''相似度 = len(集合1 & 集合2) / sqrt( len(集合1) * len(集合2) * 1.0)'''
            similarity[i][j] += cuv / math.sqrt(commonMultiple[i] * commonMultiple[j] * 1.0)

    return similarity


## 相似用户的物品
def item_recommend(userId, train, similarity, k):
    '''3.相似用户的物品'''
    rank = dict()
    for i, pi in train[userId].items():
        for j, wj in sorted(similarity[i].items(), key=lambda x: x[1], reverse=True)[:k]:
            if j in train[userId]:
                continue
            elif rank.get(j, -1) == -1:
                rank[j] = 0
            rank[j] += float.fromhex(pi) * wj
    return rank
